export function secondsToMMSS(secs: number) {
  const min = Math.floor(secs / 60);
  const secsLeft = secs % 60;
  return `${min < 10 ? "0" : ""}${min}:${secsLeft < 10 ? "0" : ""}${secsLeft}`;
}
