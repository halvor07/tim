import { useRef } from "react";
import "./NumberInput.scss";

interface Props {
  label: string;
  value: number;
  displayValue?: number | string;
  onChange: (change: number) => void;
}

export default function NumberInput({
  label,
  value,
  displayValue,
  onChange,
}: Props) {
  const timer = useRef<number>();

  function startChangingValue(
    event:
      | React.MouseEvent<HTMLButtonElement>
      | React.TouchEvent<HTMLButtonElement>
  ) {
    if (timer.current) clearInterval(timer.current);
    const decrease = event.currentTarget.value === "decrease";
    timer.current = setTimeout(() => {
      timer.current = setInterval(() => {
        onChange(decrease ? -1 : 1);
      }, 100);
    }, 500);
  }

  function stopChangingValue() {
    if (timer.current) clearInterval(timer.current);
  }

  return (
    <div>
      <div className="number-input__label">{label}</div>
      <div className="number-input__buttons-and-value">
        <button
          onClick={() => onChange(-1)}
          value="decrease"
          onMouseDown={startChangingValue}
          onTouchStart={startChangingValue}
          onMouseUp={stopChangingValue}
          onTouchEnd={stopChangingValue}
          onMouseLeave={stopChangingValue}
          onContextMenu={(event) => event.preventDefault()}
          disabled={value <= 1}
        >
          -
        </button>
        {displayValue ?? value}
        <button
          onClick={() => onChange(1)}
          onMouseDown={startChangingValue}
          onTouchStart={startChangingValue}
          onMouseUp={stopChangingValue}
          onTouchEnd={stopChangingValue}
          onMouseLeave={stopChangingValue}
          onContextMenu={(event) => event.preventDefault()}
        >
          +
        </button>
      </div>
    </div>
  );
}
