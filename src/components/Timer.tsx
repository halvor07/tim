import { useEffect, useRef, useState } from "react";
import { Actions, togglePause, stop } from "../state";
import { secondsToMMSS } from "../utils";
import { countdown5sec } from "./sounds";

const countdown5 = new Audio(countdown5sec);

interface Props {
  sets: number;
  participants: number;
  work: number;
  rest: number;
  started: Date;
  paused?: Date;
  dispatch: React.Dispatch<Actions>;
}

export default function Timer({
  sets,
  participants,
  work,
  rest,
  started,
  paused,
  dispatch,
}: Props) {
  const wakeLock = useRef<WakeLockSentinel>();
  const setTick = useState(true)[1];

  useEffect(() => {
    if ("wakeLock" in navigator)
      navigator.wakeLock
        .request("screen")
        .then((lock) => (wakeLock.current = lock))
        .catch(() => {
          alert("I'm not allowed to keep your screen awake");
        });
    else alert("Unable to keep your screen awake");

    return () => {
      if (wakeLock.current)
        void wakeLock.current.release().then(() => {
          wakeLock.current = undefined;
        });
    };
  }, []);

  const calculatedStuff = calculateStuff(
    started,
    sets,
    participants,
    rest,
    work
  );
  const { stage, secsLeft } = calculatedStuff;
  const timeLeft = secondsToMMSS(secsLeft);

  useEffect(() => {
    if (paused) countdown5.pause();
    if (stage === "done" || paused) return;

    const id = setInterval(() => {
      setTick((tick) => !tick);
    }, 200);
    return () => clearInterval(id);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [stage, paused]);

  useEffect(() => {
    if (!countdown5.paused) return;
    if (secsLeft === 5) {
      countdown5.currentTime = 0;
      void countdown5.play();
    } else if (secsLeft === 3) {
      countdown5.currentTime = 1.96; // 1.99 works perfectly in Chrome, but not in FF...
      void countdown5.play();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [secsLeft]);

  if (stage === "done") {
    return (
      <div className="app timer" onClick={() => dispatch(stop())}>
        <p>Done!</p>
      </div>
    );
  }

  if (stage === "prepare") {
    return (
      <div className="app timer prepare">
        <p>Prepare!</p>
        <p className="timer__left">{timeLeft}</p>
      </div>
    );
  }

  return (
    <div
      className={`app timer ${stage === "work" ? "timer--working" : ""}`}
      onClick={() => dispatch(togglePause())}
    >
      <p>{getSetText(sets, calculatedStuff)}</p>
      <p>{getParticipantText(calculatedStuff, participants)}</p>
      <p>{stage === "work" ? "Work!" : "Rest"}</p>
      <p className="timer__left">{timeLeft}</p>
      {paused && <div className="pause">PAUSED</div>}
    </div>
  );
}

type CalculatedStuff =
  | { stage: "prepare"; secsLeft: number }
  | { stage: "done"; secsLeft: number }
  | {
      set: number;
      participant: number;
      stage: "work";
      secsLeft: number;
    }
  | {
      set: number;
      nextSet: number | undefined;
      nextParticipant: number;
      stage: "rest";
      secsLeft: number;
    };

function calculateStuff(
  started: Date,
  sets: number,
  participants: number,
  rest: number,
  work: number
): CalculatedStuff {
  const msPassed = new Date().getTime() - started.getTime();
  const secsPassed = msPassed / 1000;
  if (secsPassed < 0)
    return {
      stage: "prepare",
      secsLeft: Math.ceil(Math.abs(secsPassed)),
    };
  const endTime =
    (rest + work) * sets - (rest - work * (participants - 1)) / participants;
  if (secsPassed > endTime) return { stage: "done", secsLeft: 0 };
  const set = Math.floor(secsPassed / (work + rest));
  const setTime = secsPassed % (work + rest);
  const pLength = (work + rest) / participants; // participant length
  const participant = Math.floor(setTime / pLength);
  const pTime = setTime % pLength;
  const working = pTime < work;
  const secsLeft = Math.ceil(working ? work - pTime : pLength - pTime);
  if (working) return { set, participant, stage: "work", secsLeft };
  const nextParticipant = (participant + 1) % participants;
  const nextSet = nextParticipant === 0 ? (set + 1) % sets : undefined;
  return { set, nextSet, nextParticipant, stage: "rest", secsLeft };
}

function getSetText(sets: number, calculatedStuff: CalculatedStuff) {
  const { stage } = calculatedStuff;
  if (stage === "rest" && calculatedStuff.nextSet)
    return `Next set: ${calculatedStuff.nextSet + 1}/${sets}`;
  if (stage === "work" || stage === "rest")
    return `Set: ${calculatedStuff.set + 1}/${sets}`;
}

function getParticipantText(
  calculatedStuff: CalculatedStuff,
  participants: number
) {
  const { stage } = calculatedStuff;
  if (participants === 1) return;
  if (stage === "work")
    return `Participant: ${calculatedStuff.participant + 1}/${participants}`;
  if (stage === "rest")
    return `Next participant: ${
      calculatedStuff.nextParticipant + 1
    }/${participants}`;
}
