export function typedAction<T extends string>(type: T): { type: T };
export function typedAction<T extends string, P>(
  type: T,
  payload: P
): { type: T; payload: P };
export function typedAction(type: string, payload?: unknown) {
  return { type, payload };
}

interface State {
  sets: number;
  work: number;
  rest: number;
  participants: number;
  started?: Date;
  paused?: Date;
}

export const initialState: State = {
  sets: Number(localStorage.getItem("sets")) || 6,
  work: Number(localStorage.getItem("work")) || 5,
  rest: Number(localStorage.getItem("rest")) || 25,
  participants: Number(localStorage.getItem("participants")) || 2,
  started: undefined,
  paused: undefined,
};

export const changeNumberSetting = (
  stateName: "sets" | "work" | "rest" | "participants",
  change: number
) => typedAction("changeNumberSetting", { stateName, change });
export const start = () => typedAction("start");
export const togglePause = () => typedAction("togglePause");
export const stop = () => typedAction("stop");

export type Actions = ReturnType<
  typeof changeNumberSetting | typeof start | typeof togglePause | typeof stop
>;

export function reducer(state: State, action: Actions) {
  switch (action.type) {
    case "changeNumberSetting": {
      const { change, stateName } = action.payload;
      if (change < 0 && state[stateName] === 1) return state;
      const newStateValue = state[stateName] + change;
      localStorage.setItem(stateName, newStateValue.toString());
      return { ...state, [stateName]: newStateValue };
    }
    case "start": {
      const started = new Date(new Date().getTime() + 10000);
      return {
        ...state,
        started,
      };
    }
    case "togglePause":
      if (!state.started) return state;
      if (state.paused) {
        const pauseDuration = new Date().getTime() - state.paused.getTime();
        const started = new Date(state.started.getTime() + pauseDuration);
        return { ...state, paused: undefined, started };
      }
      return { ...state, paused: new Date() };
    case "stop":
      return { ...state, started: undefined };
    default:
      throw new Error();
  }
}
