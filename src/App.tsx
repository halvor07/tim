import { useReducer } from "react";
import { secondsToMMSS } from "./utils";
import { initialState, reducer, changeNumberSetting, start } from "./state";
import Timer from "./components/Timer";
import NumberInput from "./components/NumberInput";
import "./App.css";

function App() {
  const [state, dispatch] = useReducer(reducer, initialState);
  const { sets, work, rest, participants } = state;
  const { started, paused } = state;

  const restBetweenParticipants =
    (rest - work * (participants - 1)) / participants;

  /* const kontroll =
    restBetweenParticipants * participants + work * (participants - 1); */

  if (started) {
    return (
      <Timer
        sets={sets}
        participants={participants}
        work={work}
        rest={rest}
        started={started}
        paused={paused}
        dispatch={dispatch}
      />
    );
  }

  return (
    <div className="app settings">
      <NumberInput
        label="Sets"
        value={sets}
        onChange={(change) => dispatch(changeNumberSetting("sets", change))}
      />
      <NumberInput
        label="Work"
        value={work}
        displayValue={secondsToMMSS(work)}
        onChange={(change) => dispatch(changeNumberSetting("work", change))}
      />
      <NumberInput
        label="Rest"
        value={work}
        displayValue={secondsToMMSS(rest)}
        onChange={(change) => dispatch(changeNumberSetting("rest", change))}
      />
      <NumberInput
        label="Participants"
        value={participants}
        onChange={(change) =>
          dispatch(changeNumberSetting("participants", change))
        }
      />
      <div>
        Rest after each participant:
        <br />
        {Math.ceil(restBetweenParticipants) !== restBetweenParticipants && "~"}
        {Math.ceil(restBetweenParticipants)} seconds
      </div>
      <button onClick={() => dispatch(start())}>Start</button>
    </div>
  );
}

export default App;
